// Sem promisse

const http = require('http')

const getTurma = (letra, callback) => {
    let url = `http://files.cod3r.com.br/curso-js/turma${letra}.json`
    let response = ''
    http.get(url, res => {
        res.on('data', data => {
            response += data
        })

        res.on('end', () => {
            callback(JSON.parse(response))
        })
    })

}

let nomes = []
getTurma('A', alunos => {
    nomes = nomes.concat(alunos.map(a => {return `A: ${a.nome}`}))
    getTurma('B', alunos => {
        nomes = nomes.concat(alunos.map(a => {return `B: ${a.nome}`}))
        getTurma('C', alunos => {
            nomes = nomes.concat(alunos.map(a => {return `C: ${a.nome}`}))
            console.log(nomes)
        })
    })
    
})