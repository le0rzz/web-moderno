// Sem promisse

const http = require('http')

const getTurma = letra => {
    let url = `http://files.cod3r.com.br/curso-js/turma${letra}.json`

    return new Promise((resolve, reject) => {
        http.get(url, res => {
            let response = ''
            res.on('data', data => {
                response += data
            })
    
            res.on('end', () => {
                try{
                    resolve(JSON.parse(response))
                }catch(e){
                    reject(e)
                }
            })
        })
    })
}

// let nomes = []
// getTurma('A', alunos => {
//     nomes = nomes.concat(alunos.map(a => {return `A: ${a.nome}`}))
//     getTurma('B', alunos => {
//         nomes = nomes.concat(alunos.map(a => {return `B: ${a.nome}`}))
//         getTurma('C', alunos => {
//             nomes = nomes.concat(alunos.map(a => {return `C: ${a.nome}`}))
//             console.log(nomes)
//         })
//     })
    
// })


Promise.all([getTurma('A'), getTurma('B'), getTurma('D')])
    .then(turmas => [].concat(...turmas))
    .then(alunos => alunos.map(aluno => {return aluno.nome}))
    .then(log => console.log(log))
    .catch(e => console.log(e))