let comparaComThis = function (param) {
    console.log(this === param)
}

comparaComThis(global)

const obj = {
    at1: 'attibute1'
}
comparaComThis = comparaComThis.bind(obj)
comparaComThis(global)
comparaComThis(obj)
console.log(obj)


const obj = {
    at1: 'attibute1'
}
let comparaComThisArrow = param => console.log(this === param)
comparaComThisArrow(global)
comparaComThisArrow(module.exports)

comparaComThisArrow = comparaComThisArrow.bind(obj)
comparaComThisArrow(obj)
comparaComThisArrow(module.exports)


// Via função contrutora é a única forma de fazer o this referenciar o proprio objeto com uma função arrow
function MeuObjeto() {
    this.comparaComThisArrow = param => console.log(this === param) 
}
const obj2 = new MeuObjeto()
obj2.comparaComThisArrow(obj2) // true
obj2.comparaComThisArrow(module.exports)