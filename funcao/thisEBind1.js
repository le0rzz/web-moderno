const pessoa = {
    saudacao: 'Bom dia!',
    falar(a) {
        console.log('a',a)
        console.log(this.saudacao)
    }
}

saudacao = 'ok'
pessoa.falar()
const falar = pessoa.falar
falar() // conflito entre paradigmas: funcional e OO

const falarDePessoa = pessoa.falar.bind(pessoa)
falarDePessoa('aaaaa')