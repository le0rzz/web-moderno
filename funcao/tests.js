var tableless = {
    init: (artigos) => {
        this.artigos = artigos;
    },
    p: () => console.log(this.artigos)
};
tableless.init('um')
tableless.p()
var tableless2 = tableless;


// Objeto Literal definindo namespace
var tableless = tableless || {};
// Construtor para Artigo utilizando o namespace tableless
tableless.Artigo = function (titulo) {
    this.titulo = titulo;
};
var artigo = new tableless.Artigo('Mais um artigo sobre JavaScript');
console.log(artigo.titulo)



