function Pessoa(name) {
    this.name = name

    this.falar = () => {
        console.log(this)
    }
}

p = new Pessoa('leo')
p.falar()