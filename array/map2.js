Array.prototype.map2 = function(cb){
    const res = []
    for (let i=0; i < this.length; i++){
        res.push(cb(this[i], i, this))
    }
    return res
}

const nums = [1, 2, 3, 4, 5]

// For com propósito
let resultado = nums.map2(function(e) {
    return e * 2
})

console.log(resultado)

const soma10 = e => e + 10
const triplo = e => e * 3
const paraDinheiro = e => `R$ ${parseFloat(e).toFixed(2).replace('.', ',')}`

resultado = nums.map(soma10).map(triplo).map(paraDinheiro)
console.log(resultado)