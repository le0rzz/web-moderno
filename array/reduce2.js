const alunos = [
    { nome: 'João', nota: 7.3, bolsista: true },
    { nome: 'Maria', nota: 9.2, bolsista: true },
    { nome: 'Pedro', nota: 9.8, bolsista: true },
    { nome: 'Ana', nota: 8.7, bolsista: true }
]

const todosBolsistas = alunos.map((a)=> a.bolsista).reduce(function(acumulador, aluno){
    // console.log(acumulador,aluno,acumulador && aluno)
    return acumulador && aluno
})

const algumBolsista = alunos.map((a)=> a.bolsista).reduce(function(acumulador, aluno){
    // console.log(acumulador,aluno,acumulador || aluno)
    return acumulador || aluno
})

console.log(todosBolsistas,algumBolsista)