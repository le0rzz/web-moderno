
Array.prototype.forEach2 = function forEach2(cb) {
    let arr = this
    for (i=0; i <  arr.length; i++){
        cb(arr[i], i, arr)
    }
}

const aprovados = ['Agatha', 'Aldo', 'Daniel', 'Raquel']
aprovados.forEach2(function(nome, indice) {
    console.log(`${indice + 1}) ${nome}`)
})

aprovados.forEach(nome => console.log(nome))

const exibirAprovados = aprovado => console.log(aprovado)
aprovados.forEach(exibirAprovados)